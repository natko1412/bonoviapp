# BonoviApp
Simple web app written in Django.

## Requirements

Python3, Django, Django Crisp Forms

## Usage
 `python3 manage.py runserver`

 In browser  go to localhost:8000

 Admin user: admin, password: 12345678

 Common user: korisnik, password: lozinka123

 Logs are available to admin under AuditEntries table.