from django.shortcuts import render
from django.http import HttpResponse
from .models import Bon, Person, Setting, AuditEntry
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
import random
from django.utils import timezone
from django.contrib import messages


@login_required
def home(request):
	ip = request.META.get('REMOTE_ADDR')
	if request.user.is_staff:
		return redirect("admin/")

	expiration_seconds = int(Setting.objects.filter(key="VOUCHER_EXPIRATION_SECONDS").first().value)

	bonovi = Bon.objects.filter(owner=request.user)
	exp_dates = []
	for b in bonovi:
		expiration_date = b.date_created.timestamp() + expiration_seconds

		if expiration_date < timezone.now().timestamp():
			b.delete()
			continue

		exp_dates.append(int(expiration_date - timezone.now().timestamp()))
	return render(request, 'bon_app/home.html', {'user':request.user, 'bonovi': zip(bonovi, exp_dates)})

@login_required
def generate(request):
	ip = request.META.get('REMOTE_ADDR')
	if request.user.person.remaining_activations > 0:
		random_str = str(random.randint(1000000000, 9999999999))


		while len(list(Bon.objects.filter(serial_num=random_str)))!=0:
			random_str = str(random.rand_int(1000000000, 9999999999))

		b = Bon(serial_num=random_str, owner=request.user)

		b.save()

		request.user.person.remaining_activations -= 1

		request.user.person.save()
		ip = request.META.get('REMOTE_ADDR')
		AuditEntry.objects.create(action='user_generated_voucher', ip=ip, username=request.user.username)
	else:
		AuditEntry.objects.create(action='voucher_limit_reached_generating_denied', ip=ip, username=user.username)

	return redirect("bonovi-home")

@login_required
def activate(request):
	ip = request.META.get('REMOTE_ADDR')

	expiration_seconds = int(Setting.objects.filter(key="VOUCHER_EXPIRATION_SECONDS").first().value)

	serial_num = request.POST.get('serial_num', '')
	bon = Bon.objects.filter(serial_num=serial_num)
	print("SERIAL:  " + serial_num)
	if len(list(bon)) == 0:
		return redirect("bonovi-home")
	bon = bon.first()


	expiration_date = bon.date_created.timestamp() + expiration_seconds

	if expiration_date < timezone.now().timestamp():
		print("EXPIRED")
		AuditEntry.objects.create(action='User tried activating expired voucher {}'.format(bon.serial_num), ip=ip, username=request.user.username)
		bon.delete()
		return render(request, 'bon_app/home.html', {'msgs': ['The selected voucher has expired and therefore deleted.']})


	if bon.owner == request.user:
		request.user.person.saldo += bon.value
		request.user.person.save()
		AuditEntry.objects.create(action='User activated voucher {}'.format(bon.serial_num), ip=ip, username=request.user.username)
		bon.delete()
		

	return redirect("bonovi-home")