from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='bonovi-home'),
	path('generate', views.generate, name='generate-voucher'),
	path('activate', views.activate, name='activate-voucher')
]